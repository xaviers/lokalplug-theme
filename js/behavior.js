let filters = {};

$(function () {
    $('.list-filters .default-filter').each(function(index, element) {
        filters[$(element).attr("data-filter")] = $(element).attr("data-filter-currentValue");
    });
});

$('.list-filters .filter').click(function () {
    filters[$(this).attr("data-filter")] = $(this).attr("data-filter-value");

    let uri = URI(window.location.href);

    _.forEach(filters, function(value, filter) {
        uri.removeSearch(filter);

        if(!_.isEmpty(value))
            uri.addSearch(filter, value);
    });

    window.location.href = uri.toString();
});

$('.sort-filter').click(function () {
    let uri = URI(window.location.href);

    uri.removeSearch("sort");
    uri.addSearch("sort", $(this).attr("data-sort"));

    window.location.href= uri.toString();
});