<?php
$title = "Signup - Lokal Plug";
$navActiveIndex = -1;
require 'template/header.php';
?>

<div class="container-fluid main-container">
    <div class="col-lg-8 col-lg-offset-2 content">
        <div class="col-xs-12">
            <div class="content-container">
                <div class="page-header section-title">
                    <h1><span class="fulltape fulltape-icon"></span> Signup
                        <small>Whether you are an artist or a music lover create your account now</small>
                    </h1>
                </div>
                <div class="col-sm-8 col-sm-offset-2 form-container authentification-form-container">
                    <form role="form">
                        <div class="form-group">
                            <label for="username" class="sr-only">Username</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <input type="text" class="form-control" id="username" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="sr-only">Email</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i>
                                </div>
                                <input type="email" class="form-control" id="email" placeholder="Email address">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i>
                                </div>
                                <input type="password" class="form-control" id="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirmation" class="sr-only">Confirm Password</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i>
                                </div>
                                <input type="password" class="form-control" id="email" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <p class="caution">By clicking "Register", you confirm that you have read and agreed to our
                                <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.</p>
                            <div class="col-xs-12">
                                <div class="col-xs-12 col-md-4 col-md-offset-4">
                                    <button type="submit" class="btn btn-default g-recaptcha"
                                            data-sitekey="6LfHpSIUAAAAANUSsfx6ViMYX5M6cK7yxST9IfTd"
                                            data-callback="YourOnSubmitFn">Register
                                    </button>
                                </div>
                                <div class="col-xs-12 col-md-4 text-right">
                                    <img src="images/recaptcha.png" alt="Protected by reCaptcha" width="75px"
                                         height="22px">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'template/footer.php'; ?>
