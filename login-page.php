<?php
$title = "Login - Lokal Plug";
$navActiveIndex = -1;
require 'template/header.php';
?>

<div class="container-fluid main-container">
    <div class="col-lg-8 col-lg-offset-2 content">
        <div class="col-xs-12">
            <div class="content-container">
                <div class="page-header section-title">
                    <h1><span class="fulltape fulltape-icon"></span> Login
                    </h1>
                </div>
                <div class="col-sm-8 col-sm-offset-2 form-container authentification-form-container">
                    <form role="form">
                        <div class="form-group">
                            <label for="username" class="sr-only">Username</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i>
                                </div>
                                <input type="text" class="form-control" id="username" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i>
                                </div>
                                <input type="password" class="form-control" id="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-12 col-md-4 col-md-offset-4">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-default g-recaptcha"
                                            data-sitekey="6LfHpSIUAAAAANUSsfx6ViMYX5M6cK7yxST9IfTd"
                                            data-callback="YourOnSubmitFn">Login
                                    </button>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-4 text-right">
                                <img src="images/recaptcha.png" alt="Protected by reCaptcha" width="75px" height="22px">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'template/footer.php'; ?>
