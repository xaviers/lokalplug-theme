<?php
$title = "Bruce Little - Lokal Plug";
$navActiveIndex = 2;
require 'template/header.php';

function includeMixtapeCard($title, $artist, $poster, $avatar)
{
    $mixtapeTitle = $title;
    $mixtapeArtist = $artist;
    $mixtapePoster = $poster;
    $mixtapeArtistAvatar = $avatar;

    include 'template/mixtape-card.php';
}

?>

<div class="container-fluid main-container">
    <div class="col-lg-8 col-lg-offset-2 content">
        <div class="col-xs-12 col-sm-10">
            <div class="content-container">
                <div class="artist-infos">
                    <div class="artist-infos-background"
                         style="background: url('https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/18301491_1478829748834181_1514752967733841350_n.jpg?oh=15645dd1bacc46278f7dde3dcf11e415&oe=59AA7716');background-size: cover;"></div>
                    <div class="col-sm-3 artist-cover-container">
                        <img src="images/samples/brucelittle.jpg">
                    </div>
                    <div class="col-sm-9 artist-infos-container">
                        <h3 class="artist-title">Bruce Little</h3>
                        <p><i class="fa fa-star" aria-hidden="true"></i> 3.5/5 stars</p>
                        <p><i class="fa fa-cloud-download" aria-hidden="true"></i> 8833 downloads</p>
                        <p><i class="fa fa-calendar" aria-hidden="true"></i> July 14, 2016 (last official activity)</p>
                    </div>
                </div>
                <div class="page-header section-title">
                    <h1><span class="fulltape fulltape-icon"></span> Mixtapes</h1>
                </div>
                <div class="jumbotron">
                    <div class="row card-list-container">
                        <div class="col-sm-2 col-xs-4">
                            <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                        </div>
                        <div class="col-sm-2 col-xs-4">
                            <?php includeMixtapeCard("Street Monopoly 2", "Bruce Little", "street_monopoly_2-bruce_little.jpg", "brucelittle.jpg") ?>
                        </div>
                        <div class="col-sm-2 col-xs-4">
                            <?php includeMixtapeCard("Street Monopoly", "Bruce Little", "street_monopoly-bruce_little.jpg", "brucelittle.jpg") ?>
                        </div>
                    </div>
                </div>
                <div class="page-header section-title">
                    <h1><span class="fulltape fulltape-icon"></span> Featuring Mixtapes</h1>
                </div>
                <div class="jumbotron">
                    <div class="row card-list-container">
                        <div class="col-sm-2 col-xs-4">
                            <?php includeMixtapeCard("Street Monopoly Origins", "Bruce Little", "street_monopoly_origins-bruce_little.png", "brucelittle.jpg") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require 'template/sidebar.php'; ?>
    </div>
</div>

<?php require 'template/footer.php'; ?>
