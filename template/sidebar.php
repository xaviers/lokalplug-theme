<div class="col-sm-2 sidebar">
    <div class="row">
        <a class="btn btn-default wrap-whitespace upload-now-button">
            <i class="fa fa-cloud-upload" aria-hidden="true"></i> Upload Your Mixtape
        </a>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-cloud-download" aria-hidden="true"></i> Most Downloaded
                    This Week</h3>
            </div>
            <div class="panel-body">
                <div class="col-xs-10 col-xs-offset-1 mixtape-card-min">
                    <div class="mixtape-cover"
                         style="background-image: url('images/samples/street_monopoly_origins-bruce_little.png');">
                        <a href="#"></a></div>
                    <div class="mixtape-card-caption">
                        <a href="#">Street Monopoly Origins</a>
                    </div>
                </div>
                <div class="col-xs-10 col-xs-offset-1 mixtape-card-min">
                    <div class="mixtape-cover"
                         style="background-image: url('images/samples/ydd-tiitof.png');">
                        <a href="#"></a></div>
                    <div class="mixtape-card-caption">
                        <a href="#">Young Drug Dealers</a>
                    </div>
                </div>
                <div class="col-xs-10 col-xs-offset-1 mixtape-card-min">
                    <div class="mixtape-cover"
                         style="background-image: url('images/samples/true_story-ed_style.jpg');"><a
                            href="#"></a></div>
                    <div class="mixtape-card-caption">
                        <a href="#">True Story</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>