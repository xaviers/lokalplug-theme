<?php
if (!isset($artist)) $artist = "";
if (!isset($artistAvatar)) $artistAvatar = "";
?>

<div class="thumbnail list-card artist-card">
    <div class="card-cover">
        <a href="#">
            <div class="card-cover-bg"
                 style="background-image: url('images/samples/<?php echo $artistAvatar ?>');"></div>
            <img alt="artist_cover"
                 src="images/samples/<?php echo $artistAvatar ?>"></a>
    </div>
    <div class="caption">
        <p class="card-title"><a href="#"><?php echo $artist ?></a></p>
        <hr/>
        <p style="font-size:10px"><i class="fa fa-clock-o"
                                     aria-hidden="true"></i> 9 months ago
        </p>
        <p style="font-size:10px"><i class="fa fa-cloud-download"
                                     aria-hidden="true"></i> 8833 d/l</p>
        <p style="font-size:10px">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half" aria-hidden="true"></i>
            <i class="fa fa-star-o" aria-hidden="true"></i>
        </p>
    </div>
</div>
