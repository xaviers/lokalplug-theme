<div class="container-fluid footer">
    <div class="col-xs-1 col-lg-offset-2 text-center">
        <img class="brand-logo" alt="Lokal Plug Logo" src="images/logo.svg">
    </div>
</div>

<script src="js/jquery/jquery-3.1.1.js"></script>
<script src="js/bootstrap/bootstrap-3.3.5.min.js"></script>
<script src="js/lodash/lodash.min.js"></script>
<script src="js/uri/URI.min.js"></script>
<script src="js/layout.js"></script>
<script src="js/behavior.js"></script>
</body>
</html>